package paprika.ktest

class AssertTestCase<T : Any?>(val actual: T, private var message: String? = null) : TestCase {
    private var action: ((T) -> String)? = null
    override fun validate(): TestResult {
        return try {
            val result = action?.invoke(actual) ?: error("assert has not action")
            TestResult(true, message ?: result)
        } catch (e: Throwable) {
            TestResult(false, message ?: e.message, e)
        }
    }

    fun test(action: (T) -> String) {
        this.action = action
    }
}

infix fun <T> AssertTestCase<T>.equal(expect: T) = test {
    if (actual == expect)
        return@test "expect `${actual}` to be `$expect`"
    throw AssertionError("expect `${actual}` to be `$expect`")
}

val <T> AssertTestCase<T>.be
    get() = this

inline fun <reified T> AssertTestCase<*>.instanceOf() = test {
    if (actual is T)
        return@test "expect `${actual}` to be instance of `${T::class.simpleName}`"
    throw AssertionError("expect `${actual}` to be instance of `${T::class.simpleName}`")
}


infix fun AssertTestCase<FloatArray>.comparableTo(expect: FloatArray) = test {
    if (actual.contentEquals(expect))
        return@test "expect `${actual.joinToString(",")}` to be comparableTo `${expect.joinToString(",")}`"
    throw AssertionError("expect `${actual}` to be `$expect`")
}

infix fun <T> AssertTestCase<List<T>>.comparableTo(expect: List<T>) = test {
    if (actual.containsAll(expect))
        return@test "expect `${actual.joinToString(",")}` to be comparableTo `${expect.joinToString(",")}`"
    throw AssertionError("expect `${actual}` to be `$expect`")
}

