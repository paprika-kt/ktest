package paprika.ktest

interface TestCase {
    fun validate(): TestResult
}