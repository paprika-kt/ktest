package paprika.ktest

data class TestResult(
    val success: Boolean,
    val message: String? = null,
    val exception: Throwable? = null,
    val child: List<TestResult>? = null
)