package paprika.ktest

open class CompositeTestCase(protected val message: String? = null) : TestCase {
    private val tests = arrayListOf<TestCase>()

    override fun validate(): TestResult {
        val list = tests.map { it.validate() }
        val success = list.all { it.success }
        return TestResult(success, message, child = list)
    }

    fun <T :TestCase> T.add(): T {
        tests.add(this)
        return this
    }

}