package paprika.ktest

class AssertTrueTestCase(val message: String? = null, val action: () -> Boolean) : TestCase {
    override fun validate(): TestResult {
        return try {
            TestResult(action(), message)
        } catch (e: Throwable) {
            TestResult(false, message, e)
        }
    }
}