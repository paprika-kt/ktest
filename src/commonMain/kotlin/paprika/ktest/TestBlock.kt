package paprika.ktest

open class TestBlock(message: String? = null, val action: TestBlock.() -> Unit) : CompositeTestCase(message) {

    /**
     * Create a child test block
     *
     * ```kotlin
     * "Test name" { [...] }
     * ```
     */
    operator fun String.invoke(block: TestBlock.() -> Unit) =
        TestBlock(this, block).add()

    /**
     * For quickly disable test block
     *
     * ```kotlin
     * "Test name" - { [...] }
     * ```
     */
    operator fun String.minus(block: TestBlock.() -> Unit) {}

    inline fun <reified T : Throwable> String.fail(crossinline block: TestBlock.() -> Unit) =
        TestBlock(this) {
            try {
                block()
                throw AssertionError()
            } catch (e: Throwable) {
                if (e !is T) throw AssertionError()
            }
        }.add()

    inline fun <reified T : Throwable> fail(crossinline block: TestBlock.() -> Unit) =
        TestBlock() {
            try {
                block()
                throw AssertionError()
            } catch (e: Throwable) {
                if (e !is T) throw AssertionError()
            }
        }.add()

    fun assert(message: String, block: () -> Boolean) = AssertTrueTestCase(message, block).add()
    fun <T> String.assert(actual: T, block: T.() -> Boolean) = AssertTrueTestCase(this) { actual.run(block) }.add()
    infix fun Any?.shouldBe(expect: Any?) = AssertTestCase(this).add().equal(expect)
    val <T : Any> T.should: AssertTestCase<T>
        get() = AssertTestCase(this).add()

    override fun validate(): TestResult {
        return try {
            action()
            super.validate()
        } catch (e: AssertionError) {
            TestResult(false, message, e, child = listOf(TestResult(false, e.message)))
        } catch (e: Throwable) {
            TestResult(false, message, e)
        }
    }
}