package paprika.ktest

fun describe(action: TestBlock.() -> Unit) {
    val helper = TestBlock(action = action)
    val result = helper.validate()
    printResult(result)
    if (!result.success)
        throw AssertionError()
}

fun describe(message: String, action: TestBlock.() -> Unit) {
    val helper = TestBlock(message, action = action)
    val result = helper.validate()
    printResult(result)
    if (!result.success)
        throw AssertionError()
}

fun xdescribe(action: TestBlock.() -> Unit) {

}

fun printResult(result: TestResult, showAll: Boolean = false, level: Int = 0) {
    val (success, message, exp, child) = result
    val space = (0 until level).joinToString("") { "  " }
    if (success && message != null)
        println("$space✅ $message")
    else if (!message.isNullOrEmpty())
        println("$space❌ $message")
    if (exp != null && !exp.message.isNullOrEmpty())
        println("$space  ${exp.message?.replace("\n", "\n$space")}")
    if (!success || message == null || showAll)
        child?.forEach { printResult(it, showAll, if (message != null) level + 1 else level) }
}