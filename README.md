ktest
=====

Only a sugar syntax for test (work on all platform)

To review in feature

Feature
=======
* Execute all test/assert before fail


Exemple
=======
```kotlin
fun Vector2fLength() = describe {
    "Valid vectorOf(10f, 10f) length" {
        vectorOf(10f, 10f).apply {
            length shouldBe 14.142135623730951f
        }
    }
}
```

print:
```
✅ Valid vectorOf(10f, 10f) length
  ✅ expect `14.142136` to be `14.142136` (14.142136==14.142136)
```